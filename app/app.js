'use strict';

/**
 * @ngdoc overview
 * @name Hubspot Code Exercise
 * @description
 * # Hubspot Code Exercise
 *
 * Main module of the application.
 */
var app = angular.module('app', ['ngRoute']);

app.config(['$routeProvider', function ($routeProvider) {
		$routeProvider
			.when('/', {
			    templateUrl: 'app/testimonial/testimonial-view.html',
			})
			.when('/cta', {
			    templateUrl: 'app/cta/cta-view.html',
			    controller: 'CtaCtrl'
			})
			.when('/filterable-content', {
			    templateUrl: 'app/filterable/filterable-view.html',
			    controller: 'FilterableCtrl'
			})
			.otherwise({
			    redirectTo: '/'
			});
}]);
