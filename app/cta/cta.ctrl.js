'use strict';

app.controller('CtaCtrl', ['$scope', 'dataFactory', function($scope, dataFactory) {


//set up base url
$scope.config = {
	url: 'http://api.icndb.com/jokes/random',
}

//display random joke when user clicks on getJoke
$scope.getJoke = function() {
   dataFactory.getData(function(chuckNorris){
		if(chuckNorris != 'error'){
			$scope.jokes = chuckNorris;
		} else {
			$scope.apiError = true;
		}
  }, $scope.config.url);
}

}]);