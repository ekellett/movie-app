'use strict';

app.controller('FilterableCtrl', ['$scope', 'dataFactory', function($scope, dataFactory) {
$scope.years = [];
$scope.genres = [];
$scope.displayGenre = false;
$scope.displayYear = false;


//set up base url
$scope.config = {
	url: 'assets/data/data.json',
}

	dataFactory.getData(function(moviesData){
    //if there are no errors
		if(moviesData != 'error'){
			$scope.media = moviesData;
			angular.forEach($scope.media, function(movies, index){
				$scope.movies = movies;
                //creating an array for years to avoid duplication
                angular.forEach($scope.movies, function(movie, index){
                  //if year is not already in array add 
                	if($scope.years.indexOf(movie.year) == -1){
                		$scope.years.push(movie.year); 
                    //sort array with earliest year first
                		$scope.years.sort();
                	}  
                	$scope.genre = movie.genre;
                  //creating an array for genres to avoid duplication
                	angular.forEach($scope.genre, function(genre, index){
                    //if genre is not already in array add 
                			if($scope.genres.indexOf(genre) == -1){
                			$scope.genres.push(genre);
                      //sort alphabetically
                			$scope.genres.sort();
                		}  
                	});
                	 
            	});
            });
            //display error if problem connecting to data
        		} else {
        			$scope.apiError = true;
        		}
  	}, $scope.config.url);

    //clear all filter
  	$scope.clearFilter = function() {
      $scope.searchBox = "";
      $scope.type = {};
      $scope.byGenre =  "";
      $scope.byYear =  "";
    };

}]);