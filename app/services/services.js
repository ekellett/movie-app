'use strict';

app.factory('dataFactory', function($http) {
	return {
		getData: function(callback, url){
			//make get request to api
		 	$http({
			  method: 'GET',
			  url: url
			}).then(function successCallback(response) {
				//trigger callback function on successful response
			    callback(response.data);
			}, function errorCallback() {
				//send error message back if request fails
			    callback('error');
			});
		}
	}
});