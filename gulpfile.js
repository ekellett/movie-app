var gulp = require('gulp');
var sass = require('gulp-sass');
var webserver  = require('gulp-webserver');



gulp.task('sass', function() {
  gulp.src('./assets/styles/scss/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('./assets/styles/css'));
});


gulp.task('webserver', function() {
gulp.src('.')
    .pipe(webserver({
      livereload: true,
      open: true,
      port: 8080
    }));
});

// The default task (called when we run `gulp` from cli)
gulp.task('default', ['webserver', 'sass'], function() {

});
